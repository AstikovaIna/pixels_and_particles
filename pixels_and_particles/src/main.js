import Effect from './classEffect.js'
window.addEventListener('load', function () {
  const canvas = document.getElementById('canvas1');
  const ctx = canvas.getContext('2d');
  canvas.width = window.innerWidth;
  canvas.height = window.innerHeight;


  const image = document.getElementById('image1');


  // Get the remote image as a Blob with the fetch API
  fetch(image.src)
    .then((res) => res.blob())
    .then((blob) => {
      const reader = new FileReader();
      reader.onloadend = () => {
      };
      reader.readAsDataURL(blob);
    });


  const effect = new Effect(canvas.width, canvas.height);
  effect.init(ctx);

  function animate() {
    ctx.clearRect(0, 0, canvas.width, canvas.height)
    effect.draw(ctx);
    effect.update();
    requestAnimationFrame(animate);
  }
  animate();

  //warp button

  const warpButton = this.document.getElementById('warpButton');
  warpButton.addEventListener('click', function () {
    effect.warp();
  });

  //blocks button

  const blocksButton = this.document.getElementById('blocksButton');
  blocksButton.addEventListener('click', function () {
    effect.blocks();
  });

  //assemble button

  const assembleButton = this.document.getElementById('assembleButton');

  assembleButton.addEventListener('click', function () {
    effect.assemble();
  });

  //print button

  const print2DButton = this.document.getElementById('printButton');
  print2DButton.addEventListener('click', function () {
    effect.print();
  })

  //change button

  const changeButton = this.document.getElementById('changeImageButton');

  changeButton.addEventListener('click', function () {
    //working on it
  })
})
