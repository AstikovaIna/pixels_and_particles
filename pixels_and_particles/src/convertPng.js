const getBase64StringFromDataURL = (dataURL) =>
  dataURL.replace('data:', '').replace(/^.+,/, '');

const getRandomImageSrc = (items) => {
  return items[Math.floor(Math.random() * items.length)]
}

const image = document.getElementById('image1');
// let randomSrc = getRandomImageSrc(srcArray);

// image.setAttribute('src', randomSrc);

// Get the remote image as a Blob with the fetch API
fetch(image.src)
  .then((res) => res.blob())
  .then((blob) => {
    // Read the Blob as DataURL using the FileReader API
    const reader = new FileReader();
    reader.onloadend = () => {
      // console.log(reader.result);
      // Logs data:image/jpeg;base64,wL2dvYWwgbW9yZ...

      // Convert to Base64 string
      const base64 = getBase64StringFromDataURL(reader.result);
      // console.log(base64);
      // Logs wL2dvYWwgbW9yZ...
    };
    reader.readAsDataURL(blob);
  });