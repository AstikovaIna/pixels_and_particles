# Pixels and Particles Project
<img src="./pixels_and_particles/images/banner2.jpeg"/>
Open in a browser here: https://astikovaina.gitlab.io/pixels_and_particles/pixels_and_particles

The **Pixels and Particles** project is a static page.
## Table of contents
- [Pixels and Particles Project](#pixels-and-particles-project)
  - [Table of contents](#table-of-contents)
  - [What the project does](#what-the-project-does)
  - [Visuals](#visuals)
  - [What's included](#whats-included)
  - [Categories](#categories)
  - [Browser Support](#browser-support)
  - [Author](#author)
  - [Used technologies](#used-technologies)
  - [Copyright and license](#copyright-and-license)

## What the project does
- This simple project shows the power of the plain vanilla JavaScript. The project turns images into interactive animated pixels with physics (friction, easing). Also make those pixels react to mouse. With this project you can break the images apart into individual pixels and make them automatically reassemble in 4 unique different ways.


## Visuals
- Home page;
<img src="./pixels_and_particles/images/visual.jpeg" width='700px'/>

## What's included
  The project is wrote on Visual Studio Code and using:
- Javascript;
- HTML;
- CSS;
- GIT;
- ECMAScript features;

## Buttons
- Warp
- Blocks 
- Assemble
- 3Dprint

## Browser Support
At the moment, we aim to support all major web browsers. Any issue in the browsers listed below should be reported as a bug:
- Internet Explorer 10+
- Microsoft Edge 14+
- Safari 6+
- Firefox  Current versions
- Chrome   Current versions
- Opera    Current versions
- Safari iOS 7.0+
- Android 6.0+

## Author 
- [Velina Astikova](https://gitlab.com/AstikovaIna)

## Used technologies
- HTML
- CSS
- JAVASCRIPT

## Copyright and license

Code and documentation copyright 2022  [MIT License]
